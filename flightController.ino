#include "Servo.h";
#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif
//DMP variables
MPU6050 mpu;
#define INTERRUPT_PIN 2  // use pin 2 on Arduino Uno & most boards
#define LED_PIN 13 // (Arduino is 13, Teensy is 11, Teensy++ is 6)
bool blinkState = false;
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
float yprDeg[3];
//motor output variables
bool safteyShutOff = false;
//top left top right bottom left bottom right
Servo esc[4];
const int motorRange[2] = {1000,2000};
//top left top right bottom left bottom right
int motorOutputs[4] = {motorRange[0],motorRange[0],motorRange[0],motorRange[0]};
//DEFINE MOTOR PINS
#define MOTOR_TL_PIN
#define MOTOR_TR_PIN
#define MOTOR_BL_PIN
#define MOTOR_BR_PIN
//PID variables
const int PIDrange[2] = {0,250};
//top left top right bottom left bottom right
int PIDoutputs[4] = {PIDrange[0],PIDrange[0],PIDrange[0],PIDrange[0]};
//pith roll
float err[2] = {0,0};
float errSum[2] = {0,0};
float errPrev[2] = {0,0};
double tDelata;
long timePrevPID;
float PID[3] = {1.0,1.0,1.0};
//pitch roll
float target[2] = {0,0};
//interrupt for FIFO 
volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}
void setup() {
    // join I2C bus (I2Cdev library doesn't do this automatically)
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
        Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif
    mpu.initialize();
    pinMode(INTERRUPT_PIN, INPUT);
    devStatus = mpu.dmpInitialize();
//offsets for specific mpu6050
    mpu.setXGyroOffset(220);
    mpu.setYGyroOffset(76);
    mpu.setZGyroOffset(-85);
    mpu.setZAccelOffset(1788); // 1688 factory default for my test chip

    // make sure it worked (returns 0 if so)
    if (devStatus == 0) {
        // turn on the DMP, now that it's ready
        mpu.setDMPEnabled(true);

        // enable Arduino interrupt detection
        attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
        mpuIntStatus = mpu.getIntStatus();

        // set our DMP Ready flag so the main loop() function knows it's okay to use it
        dmpReady = true;

        // get expected DMP packet size for later comparison
        packetSize = mpu.dmpGetFIFOPacketSize();
    } else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
    }
    esc[0].attach(MOTOR_TL_PIN);
    esc[1].attach(MOTOR_TR_PIN);
    esc[2].attach(MOTOR_BL_PIN);
    esc[3].attach(MOTOR_BR_PIN);
    timePrevPID = micros();
}
void loop() {
    // if programming failed, don't try to do anything
    if (!dmpReady) return;

    // wait for MPU interrupt or extra packet(s) available
    while (!mpuInterrupt && fifoCount < packetSize) {
        if (mpuInterrupt && fifoCount < packetSize) {
          // try to get out of the infinite loop 
          fifoCount = mpu.getFIFOCount();
        }  
        //convert DMP data from radians to degrees use approximation to save time 
        yprDeg[0] = 57 * ypr[0];
        yprDeg[1] = 57 * ypr[1];
        yprDeg[2] = 57 * ypr[2];
        //read radio input

        //calculate PID response SIGNS NEED TO BE DETERMINED
        err[0] = target[0] - ypr[1];
        err[1] = target[1] - ypr[2];
        tDelata = (micros() - timePrevPID) / 1000000;
        PIDoutputs[0] = PID[0] *(err[0] + err[1]) + PID[1] * (errSum[0] + errSum[1]) + PID[2] * tDelata * ((errPrev[0] - err[0]) + (errPrev[1] - err[1]));
        PIDoutputs[1] = PID[0] *(err[0] + err[1]) + PID[1] * (errSum[0] + errSum[1]) + PID[2] * tDelata * ((errPrev[0] - err[0]) + (errPrev[1] - err[1]));
        PIDoutputs[2] = PID[0] *(err[0] + err[1]) + PID[1] * (errSum[0] + errSum[1]) + PID[2] * tDelata * ((errPrev[0] - err[0]) + (errPrev[1] - err[1]));
        PIDoutputs[3] = PID[0] *(err[0] + err[1]) + PID[1] * (errSum[0] + errSum[1]) + PID[2] * tDelata * ((errPrev[0] - err[0]) + (errPrev[1] - err[1]));
        for(int i = 0;i < 4; i++)
        {
            if(PIDoutputs[i] > PIDrange[1])
            {
                PIDoutputs[i] = PIDrange[1];
            }
            else if(PIDoutputs[i] < PIDrange[0])
            {
                PIDoutputs[i] = PIDrange[0];
            }
        }
        errPrev[0] = err[0];
        errPrev[1] = err[1];
        errSum[0] += err[0] * tDelata;
        errSum[1] += err[1] * tDelata;
        timePrevPID = micros();
        // mix user input
        //shutoff if out of control
        if(ypr[1] > 60 || ypr[2] > 60 || ypr[1] < -60 || ypr[1] < -60 || safteyShutOff)
        {
            safteyShutOff = true;
            esc[0].writeMicroseconds(motorRange[0]);
            esc[1].writeMicroseconds(motorRange[0]);
            esc[2].writeMicroseconds(motorRange[0]);
            esc[3].writeMicroseconds(motorRange[0]);
        }
        else
        {
        //write motor outputs
        esc[0].writeMicroseconds(motorOutputs[0]);
        esc[1].writeMicroseconds(motorOutputs[1]);
        esc[2].writeMicroseconds(motorOutputs[2]);
        esc[3].writeMicroseconds(motorOutputs[3]);
        }
    }

    // reset interrupt flag and get INT_STATUS byte
    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();

    // get current FIFO count
    fifoCount = mpu.getFIFOCount();

    // check for overflow (this should never happen unless our code is too inefficient)
    if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024) {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        fifoCount = mpu.getFIFOCount();

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
    } else if (mpuIntStatus & _BV(MPU6050_INTERRUPT_DMP_INT_BIT)) {
        // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);


    }
}